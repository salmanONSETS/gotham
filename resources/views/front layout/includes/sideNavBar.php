<div class="col-lg-3 col-md-3">
      <h1 class="display-4 text-center" style="padding-top:5%">Watch</h1>
      <div class="row leftside">
        <div class="custom-control custom-checkbox mb-3 col-lg-12">
  <input class="custom-control-input" id="customCheck1" type="checkbox">
  <label class="custom-control-label" for="customCheck1">Men's Watches</label>
  <span class="digits cat" > (3)</span>
</div>
<div class="custom-control custom-checkbox mb-3 col-lg-12">
<input class="custom-control-input" id="customCheck2" type="checkbox">
<label class="custom-control-label" for="customCheck2">Women's Watches</label>
<span class="digits cat" > (3)</span>
</div>
<div class="custom-control custom-checkbox mb-3 col-lg-12">
<input class="custom-control-input" id="customCheck3" type="checkbox">
<label class="custom-control-label" for="customCheck3">Children's Watches</label>
<span class="digits cat" > (3)</span>
</div>
<div class="custom-control custom-checkbox mb-3 col-lg-12">
<input class="custom-control-input" id="customCheck4" type="checkbox">
<label class="custom-control-label" for="customCheck4">Pocket & Fob Watches</label>
<span class="digits cat" > (3)</span>
</div>
<div class="custom-control custom-checkbox mb-3 col-lg-12">
  <input class="custom-control-input" id="customCheck5" type="checkbox" checked>
  <label class="custom-control-label" for="customCheck5">Watch Accessories</label>
  <span class="digits cat" > (3)</span>
</div>
</div>

  <h1 class="display-4 text-center">Gender</h1>
  <div class="row leftside">
    <div class="custom-control custom-checkbox mb-3 col-lg-6" style="padding-left:0px">
<input class="custom-control-input" id="customCheck11" type="checkbox">
<label class="custom-control-label" for="customCheck11">Male</label>
</div>
<div class="custom-control custom-checkbox mb-3 col-lg-6" style="padding-left:0px">
<input class="custom-control-input" id="customCheck12" type="checkbox">
<label class="custom-control-label" for="customCheck12">Female</label>
</div>
  </div>
    <h1 class="display-4 text-center">Rating</h1>
  <div class="row leftside">
  <div class="col-lg-12" style="position:relative" >
    <small>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star"></span>
    </small>
    <small class="RD"><span class="digits "> (3)</span></small>
  </div>
  <div class="col-lg-12" style="position:relative; maring:0%; padding:0%; padding-top:-2px;" >
    <small>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star"></span>
    </small>
    <small class="RD"><span class="digits "> (3)</span></small>
  </div>
  <div class="col-lg-12" style="position:relative; maring:0%; padding:0%" >
    <small>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star"></span>
    </small>
    <small class="RD"><span class="digits "> (3)</span></small>
  </div>
  <div class="col-lg-12" style="position:relative" >
    <small>
      <span class="fa fa-star checked"></span>
      <span class="fa fa-star"></span>
    </small>
    <small class="RD"><span class="digits "> (3)</span></small>
  </div>
  <div class="col-lg-12" style="position:relative"  style="padding:0%;">
    <small>
      <span class="fa fa-star checked"></span>
    </small>
    <small class="RD"><span class="digits "> (3)</span></small>
  </div>
  </div>

  <h1 class="display-4 text-center">Prize</h1>
  <div class="row leftside">
    <div class="custom-control custom-checkbox mb-3 col-lg-12">
<input class="custom-control-input" id="customCheck1" type="checkbox">
<label class="custom-control-label" for="customCheck1">under $35.00</label>
</div>
<div class="custom-control custom-checkbox mb-3 col-lg-12">
<input class="custom-control-input" id="customCheck2" type="checkbox">
<label class="custom-control-label" for="customCheck2">$35.00-$150.00</label>
</div>
<div class="custom-control custom-checkbox mb-3 col-lg-12">
<input class="custom-control-input" id="customCheck3" type="checkbox">
<label class="custom-control-label" for="customCheck3">$35.00-$150.00</label>
</div>
<div class="custom-control custom-checkbox mb-3 col-lg-12">
<input class="custom-control-input" id="customCheck4" type="checkbox">
<label class="custom-control-label" for="customCheck4">under $35.00</label>
</div>
<div class="custom-control custom-checkbox mb-3 col-lg-12">
<input class="custom-control-input" id="customCheck5" type="checkbox" checked>
<label class="custom-control-label" for="customCheck5">Over $150.00</label>
</div>
</div>
</div>
