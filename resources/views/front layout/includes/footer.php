<footer class="footer bg-warning text-center text-white pb-0">
         <div class="container-fluid">
             <div class="row">
                 <div class="col-lg-4 col-sm-12 text-center animLeft">
                     <h3 class="heading text-white revealer para"><b>Our</b> Information</h3>
                     <br>
                     <ul class="list-unstyled">



                         <li style="padding: 4%; margin-bottom: 3%; background-color: #FA8A70  ; border-radius: .25rem;"><i class="fa fa-phone"></i>&nbsp;&nbsp;&nbsp;000000000000 </li>

                         <li style="padding: 4%; margin-bottom: 3%; background-color: #FA8A70; border-radius: .25rem;"><i class="fa fa-envelope"></i>&nbsp;&nbsp;&nbsp;shop12, Mall of Gulgerb, LAHORE</li>

                         <li style="padding: 4%; margin-bottom: 3%; background-color: #FA8A70; border-radius: .25rem;"><i class="fa fa-map-marker-alt"></i>&nbsp;&nbsp;&nbsp;info@onsets.co </li>
                          <li style="padding: 4%; margin-bottom: 3%; background-color: #FA8A70; border-radius: .25rem;"><i class="fa fa-envelope"></i>&nbsp;&nbsp;&nbsp;info@onsets.co </li>

                     </ul>
                 </div>
                 <div class="col-lg-4 text-center revealer para">
                     <h3 class="heading text-white revealer para"><b>Site</b>map</h3>
                     <img src="assets/img/brand/white.png"  class="img img-responsive col-md-12"/>
                     <ul class="list-unstyled" style="margin-top: 7%;">
                         <li class="list-inline-item"><a href="#home" class="text-white">Home</a></li>
                         <li class="list-inline-item"><a href="#about" class="text-white">About</a></li>
                         <li class="list-inline-item"><a href="#product" class="text-white">Prouct-Line</a></li>
                         <li class="list-inline-item"><a href="#custom" class="text-white">Custom-Line</a></li>
                         <li class="list-inline-item"><a href="#story" class="text-white">Stories</a></li>
                         <li class="list-inline-item"><a href="#career" class="text-white">Career</a></li>
                         <li class="list-inline-item"><a href="#contact" class="text-white">Contact</a></li>

                           <li class="list-inline-item"><a class="text-white" href="" title="Dashboard">Dashboard</a>

                           <li class="list-inline-item" hidden><a href="" class="text-white">Login</a></li>
                           <li class="list-inline-item" hidden><a href="" class="text-white">Register</a></li>

                     </ul>
                 </div>
                 <div class="col-lg-4 text-center anim">
                     <h3 class="heading text-white"><b>Socialize</b> With Us</h3>
                     <br>

                     <a target="_blank" href="" class="btn btn-neutral btn-icon-only btn-twitter btn-round btn-lg" data-toggle="tooltip" data-original-title="Follow us">
                         <i class="fa fa-twitter"></i>
                     </a>

                     <a target="_blank" href="" class="btn btn-neutral btn-icon-only btn-linkedin btn-round btn-lg" data-toggle="tooltip" data-original-title="Follow us">
                         <i class="fa fa-linkedin"></i>
                     </a>

                     <a target="_blank" href="" class="btn btn-neutral btn-icon-only btn-facebook btn-round btn-lg" data-toggle="tooltip" data-original-title="Like us">
                         <i class="fa fa-facebook-square"></i>
                     </a>

                     <a target="_blank" href="" class="btn btn-neutral btn-icon-only btn-dribbble btn-lg btn-round" data-toggle="tooltip" data-original-title="Follow us">
                         <i class="fa fa-google-plus-square"></i>
                     </a>

                     <a target="_blank" href="" class="btn btn-neutral btn-icon-only btn-github btn-round btn-lg" data-toggle="tooltip" data-original-title="Star on Github">
                         <i class="fa fa-github"></i>
                     </a>

                     <hr style="border-top: .0625rem solid rgb(255, 255, 255);">
                     <div class="col-md-12">
                         <h3 class="heading text-white"><b>Subscribe</b> NewsLetter</h3>
                         <br>
                         <form role="form" method="post" action="\subcribeEmails">

                             <div class="input-group mb-4">
                                 <div class="input-group-prepend">
                                     <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                 </div>
                                 <input class="form-control" placeholder="Email" type="email" name="email">
                                 <button type="submit" class="btn btn-inline" style="background:#5e72e4; color:white">Submit</button>
                             </div>
                         </form>
                     </div>
                 </div>
                 <div class="col-md-12">
                     <h3 class="description text-white text-center">&copy;
                         <script type="text/javascript">
                             var dt = new Date();
                             document.write(dt.getFullYear() );
                         </script>
                         by <a href="http://www.onsets.co" class="" style="color:#5e72e4"><b>ONSETS<b></a>. All rights reserved | Design by
                         <a href="https://www.creative-tim.com/" class="" style="color:#5e72e4">creative-tim</a>
                     </h3>
                 </div>
             </div>
         </div>
     </footer>

     <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
   <script src='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js'></script>
   <script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js'></script>
   <!-- Include jQuery -->
       <script src="assets/js/jquery-2.2.4.min.js"></script>

       <!-- Include Multislider -->
       <script src="assets/js/multislider.min.js"></script>

       <!-- Initialize element with Multislider -->
       <script>
       $('#exampleSlider').multislider({
           interval: 7000,
           slideAll: true,
           duration: 1500
       });
       </script>
<!-- Core -->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/popper/popper.min.js"></script>
<script src="assets/vendor/bootstrap/bootstrap.min.js"></script>
<script src="assets/vendor/headroom/headroom.min.js"></script>
<!-- Optional JS -->
<script src="assets/vendor/onscreen/onscreen.min.js"></script>
<script src="assets/vendor/nouislider/js/nouislider.min.js"></script>
<script src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<!-- Argon JS -->
<script src="assets/js/argon.js?v=1.0.1"></script>
<script src="assets/js/jquery.js"></script>

<script src="assets/js/modernizr.custom.js"></script>
<script src="assets/js/jquery-transit-modified.js" type="text/javascript"></script>
<script src="assets/js/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="assets/js/layerslider.transitions.js" type="text/javascript"></script>
<script src="assets/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
 $(document).ready(function () {
     $('#layerslider').layerSlider({
         skinsPath: 'assets/skins/',
         skin: 'fullwidthdark',
         thumbnailNavigation: 'hover',
         hoverPrevNext: false,
         responsive: true,
         touchNav: true,
         responsiveUnder: 960,
         sublayerContainer: 960
     });
 });
</script>
