<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Design System for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Gotham </title>
  <!-- Favicon -->
  <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="assets/css/custom.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="assets/css/argon.css?v=1.0.1" rel="stylesheet">
  <!-- Docs CSS -->

  <link type="text/css" href="assets/css/docs.min.css" rel="stylesheet">
  <link href="assets/css/layerslider.css" rel="stylesheet" />
  <link href="assets/css/HardCodeStyle.css" rel="stylesheet" />

</head>
<body>
<?php include_once('includes/topNavBar.php');?>


    <img src="assets\img\theme\img-1-1200x1000.JPG" width="100%" height="200px">
    <br>
    <main>
      <div class="row">
        <?php include_once('includes/sideNavBar.php');?>


    <div class="col-lg-9 col-md-9 col-xs-12">
      <div class="row" style="padding:2%; padding-bottom:0%; padding-left:5%;">
        <div class="col-lg-5">
        <span class="display-4">Home/</span><span class="display-4" style="color:#FB6340">Watches
        </span>
      </div>
      <div class="col-lg-5 col-xs-12">
        <form class="form-inline" class="pull-right" style="float:right">
    <div class="form-group" >
    <label for="sorted" style="padding-right:5px; color:#FB6340">Sorted By: </label>
   <select class="form-control" id="sorted">
      <option>Default</option>
        <option>Newest</option>
       <option>High Price to Low Price</option>
       <option>Low Price to High Price</option>
     </select>
        </div><!-- /btn-group -->
      </form>
      </div>
      <div class="col-lg-2 col-xs-12" style="padding-top:3px;">
          <i class="fa fa-th-list" aria-hidden="true"></i>
          <i class="fa fa-th-large" aria-hidden="true"></i>
      </div>
      </div>
      <div class="col-lg-12" >
        <div class="row row-grid card-lift--hover cardlifthover shadow cardShadow cardShadowHS" style="padding:5%; margin-top:5%;">
          <div class="col-lg-4 col-md-4 col-xs-12" >
            <img class="card-img-top" src="assets/img/theme/team-1-800x800.jpg" alt="Card image cap">
          </div>
          <div class="col-lg-6 col-md-4 col-xs-12 grid2" >
            <h6 class="text-uppercase ProductNameLH">Product Name</h6>
            <h6 class=" text-uppercase ProductPriceLH">RS. 5000</h6>
          <small class="text-center"><del class="text-danger">4000</del> (10%)</small>
          <ul class="productDis" >
            <li>Strengthens frizzy hair</li>
            <li>Best for dry and damaged hair</li>
            <li>Provides volume and shine</li>
          </ul>

          <button type="button" class="btn btn-warning  hiddenCartBtn" style="display:block">Add to Cart</button>
            </div>
          <div class="col-lg-2" style="padding-top:5%" >
            <small  class="Stars" >
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star checked"></span>
              <span class="fa fa-star"></span>
              <span class="fa fa-star"></span>
              <span class="digits"> (3)</span>
            </small>
            <br>
            <div class="icon icon-shape icon-shape-warning rounded-circle mb-4 ">
              <i class="fa fa-flag" ></i>
            </div>
            <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle mb-4 ">
              <i class="ni ni-favourite-28"></i>
            </div>
            </div>
          </div>
          <div class="row row-grid card-lift--hover cardlifthover shadow cardShadow cardShadowHS" style="padding:5%; margin-top:5%;">
            <div class="col-lg-4 col-md-4 col-xs-12" >
              <img class="card-img-top" src="assets/img/theme/team-1-800x800.jpg" alt="Card image cap">
            </div>
            <div class="col-lg-6 col-md-4 col-xs-12 grid2" >
              <h6 class="text-uppercase ProductNameLH">Product Name</h6>
              <h6 class=" text-uppercase ProductPriceLH">RS. 5000</h6>
            <small class="text-center"><del class="text-danger">4000</del> (10%)</small>
            <ul class="productDis" >
              <li>Strengthens frizzy hair</li>
              <li>Best for dry and damaged hair</li>
              <li>Provides volume and shine</li>
            </ul>

            <button type="button" class="btn btn-warning  hiddenCartBtn" style="display:block">Add to Cart</button>
              </div>
            <div class="col-lg-2" style="padding-top:5%" >
              <small  class="Stars" >
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span>
                <span class="digits"> (3)</span>
              </small>
              <br>
              <div class="icon icon-shape icon-shape-warning rounded-circle mb-4 ">
                <i class="fa fa-flag" ></i>
              </div>
              <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle mb-4 ">
                <i class="ni ni-favourite-28"></i>
              </div>
              </div>
            </div>
          <div class="row row-grid card-lift--hover cardlifthover shadow" style="padding:5%; margin-top:0%">
            <div class="col-lg-4 col-md-4 col-xs-12" >
              <img class="card-img-top" src="assets/img/theme/team-1-800x800.jpg" alt="Card image cap">
            </div>
            <div class="col-lg-6 col-md-4 col-xs-12 grid2" >
            <h6 class="text-uppercase ProductNameLH">Product Name</h6>
            <h6 class=" text-uppercase ProductPriceLH">RS. 5000</h6>
            <small class="text-center"><del class="text-danger">4000</del> (10%)</small>
            <ul  class="productDis">
              <li>Strengthens frizzy hair</li>
              <li>Best for dry and damaged hair</li>
              <li>Provides volume and shine</li>
            </ul>

            <button type="button" class="btn btn-warning  hiddenCartBtn" style="display:block">Add to Cart</button>
              </div>
            <div class="col-lg-2" style="padding-top:5%" >
                <small  class="Stars" >
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span>
                <span class="digits"> (3)</span>
              </small>
              <br>
              <div class="icon icon-shape icon-shape-warning rounded-circle mb-4 ">
                <i class="fa fa-flag" ></i>
              </div>
              <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle mb-4 ">
                <i class="ni ni-favourite-28"></i>
              </div>
              </div>
            </div>


          </div>



        <div class="text-center" style="padding:5%;">
          <button class="btn btn-primary">View More Product</button>
        </div>

      </div>
    </div>
  </div>
  </main>



<?php include_once('includes/footer.php'); ?>
  <body>
    </html>
