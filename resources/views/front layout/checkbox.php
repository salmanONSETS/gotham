<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Design System for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Argon Design System - Free Design System for Bootstrap 4</title>
  <!-- Favicon -->
  <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="assets/css/custom.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="assets/css/argon.css?v=1.0.1" rel="stylesheet">
  <!-- Docs CSS -->

  <link type="text/css" href="assets/css/docs.min.css" rel="stylesheet">
  <link href="assets/css/layerslider.css" rel="stylesheet" />
  <link href="assets/css/HardCodeStyle.css" rel="stylesheet" />


</head>
<body>
<?php include_once('includes/topNavBar.php');?>


    <img src="assets\img\theme\img-1-1200x1000.JPG" width="100%" height="200px">
    <br>
    <main>
      <div class="row" style="padding:2%;">
            <div class="col-md-8">
                      <div class="card shadow border">
                      <div class="card-body">
                      <table class="table table-bordered table-dark">
                        <thead>
                            <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Control Section</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td>Mark</td>
                            <td>$400</td>
                            <td>
                             <input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                            </td>
                            <td><button type="button" class="btn btn-warning">More</button></td>
                            </tr>
                            <tr>
                            <td>Jacob</td>
                            <td>$400</td>
                            <td><input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                            </td>
                            <td><button type="button" class="btn btn-warning">More</button></td>
                            </tr>
                            <tr>
                            <td>Mike</td>
                            <td>$400</td>
                            <td><input type="number" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                            </td>
                            <td><button type="button" class="btn btn-warning">More</button></td>
                            </tr>
                        </tbody>
                        </table>
                      
                      </div>
                      </div>
          </div>
          
          <div class="col-md-4">
                
            <div class="card  shadow border-0"  style="">
                <div class="card-body text-center">
                <h3>Argon Design System</h3>
                <p>Argon Design >Men`s Product > Shirts</p>
                   
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span>
                <span class="digits"> (3)</span>
        <br>                   
             <div class="icon icon-shape icon-shape-warning rounded-circle md-4  ">
                <i class="fa fa-flag" ></i>
              </div>
              <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle md-4 ">
                <i class="ni ni-favourite-28"></i>
              </div>
                <br>
              <h4 class=" text-uppercase ProductPriceLH">RS. 5000</h4>  
            <medium class="text-center"><del class="text-danger">4000</del> (60%)</medium>
                 <br>
                    <div class="btn-group btn-md">
                      <button type="button" class="btn btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  style="background:#5e72e4; color:white">Seize</button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">18</a>
                        <a class="dropdown-item" href="#">20 </a>
                        <a class="dropdown-item" href="#">22 </a>
                      </div>
                         </div> 
                    <br>
                    <br>
                          <input type="text" placeholder="Quantity" class="form-control form-control-alternative " disabled="">
                    
                              
                  <a href="#" class="btn btn-warning  btn-block CartBtn mt-4" >Add to Cart</a>
                
                
              </div>
            </div>
          </div>
      

          
            </div>   
        </div>
</div><!--reviewTab-->

    </main>
<?php include_once('includes/footer.php'); ?>
    </body>
      </html>
