<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Design System for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Argon Design System - Free Design System for Bootstrap 4</title>
  <!-- Favicon -->
  <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="assets/css/custom.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="assets/css/argon.css?v=1.0.1" rel="stylesheet">
  <!-- Docs CSS -->

  <link type="text/css" href="assets/css/docs.min.css" rel="stylesheet">
  <link href="assets/css/layerslider.css" rel="stylesheet" />
  <link href="assets/css/HardCodeStyle.css" rel="stylesheet" />


</head>
<body>
<?php include_once('includes/topNavBar.php');?>


    <img src="assets\img\theme\img-1-1200x1000.JPG" width="100%" height="200px">
    <br>
    <main>
      <div class="row" style="padding:2%;">
        <div class="col-lg-12">
        <span class="display-4">Home/</span><span class="display-4"> Watches/</span><span class="display-4" style="color:#FB6340">Men`s Watch</span>
      </div>
      
        
            <div class="col-md-6">
                
            <img src="assets/img/theme/team-1-800x800.jpg" class="col-md-12">
          </div>
          <div class="col-md-2">
        
          <img src="assets/img/theme/team-1-800x800.jpg" class="col-md-12">
          <img src="assets/img/theme/team-2-800x800.jpg" class="col-md-12">
          <img src="assets/img/theme/team-3-800x800.jpg" class="col-md-12">
          <img src="assets/img/theme/team-4-800x800.jpg" class="col-md-12">

                </div>
          <div class="col-md-4">
                
            <div class="card  shadow border-0"  style="">
                <div class="card-body text-center">
                <h3>Argon Design System</h3>
                <p>Argon Design >Men`s Product > Shirts</p>
                   
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span>
                <span class="digits"> (3)</span>
        <br>                   
             <div class="icon icon-shape icon-shape-warning rounded-circle md-4  ">
                <i class="fa fa-flag" ></i>
              </div>
              <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle md-4 ">
                <i class="ni ni-favourite-28"></i>
              </div>
                <br>
              <h4 class=" text-uppercase ProductPriceLH">RS. 5000</h4>  
            <medium class="text-center"><del class="text-danger">4000</del> (60%)</medium>
                 <br>
                    <div class="btn-group btn-md">
                      <button type="button" class="btn btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  style="background:#5e72e4; color:white">Seize</button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">18</a>
                        <a class="dropdown-item" href="#">20 </a>
                        <a class="dropdown-item" href="#">22 </a>
                      </div>
                         </div> 
                    <br>
                    <br>
                          <input type="text" placeholder="Quantity" class="form-control form-control-alternative " disabled="">
                    
                              
                  <a href="#" class="btn btn-warning  btn-block CartBtn mt-4" >Add to Cart</a>
                
                
              </div>
            </div>
          </div>
      <div class="col-lg-12">
        <div class="nav-wrapper">
    <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
        <li class="nav-item">
            <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="fa fa-info-circle mr-2"></i>Description</a>
        </li>
        <li class="nav-item" >
            <a  style="padding-left:0px;" class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="fa fa-commenting mr-2"></i>Review</a>
        </li>
    </ul>
</div>

<div class="reviewTab">

        <div class="tab-content" id="myTabContent">

            <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                <div class=" shadow card card-body  card-body">
                <p class="description">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth.</p>
                <p class="description">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse.</p>
             
                  </div><!--cardBody-->
            </div><!--1tap-->
   
            <div class="tab-pane fade show active" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                <div class=" shadow card card-body  card-body">
                    <div class="col-md-12">
                        <div class="row">
                        <div class="col-md-6">
                        <h3>Salman Sajid</h3>
                        </div>
                        <div class="col-md-6 text-right">
                             <small>
                              <span class="fa fa-star checked "></span>
                              <span class="fa fa-star checked "></span>
                              <span class="fa fa-star checked "></span>
                              <span class="fa fa-star checked "></span>
                              <span class="fa fa-star checked "></span>
                            </small>
                        </div>
                            <div class="col-md-12">
                            <p>My name is salman sajid and i am 23 year old and bla bla bla bla bla bla bla bla bla bal </p>
                            </div>
                        </div>
                    </div>
                    </div>
                  </div><!--cardBody-->
            </div>
         </div>
          
                  <div class="col-lg-12">
        <div class="row row-grid" style="padding:5%">
          <div class="col-lg-4 col-md-4 col-xs-12" >
            <div class="card card-lift--hover cardlifthover shadow border-0" id="mm" style="">

              <small class="review" style=" line-height: 0.6; " >
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span>
                <span class="digits"> (3)</span>
              </small>
              <div class="icon icon-shape icon-shape-warning rounded-circle mb-4 PIC">
                <i class="fa fa-flag" ></i>
              </div>
              <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle mb-4 PIC">
                <i class="ni ni-favourite-28"></i>
              </div>


                <img class="card-img-top" src="assets/img/theme/team-1-800x800.jpg" alt="Card image cap">
                <div class="card-body text-center">

                  <h6 class=" text-uppercase text-center ProductNameLH" >Product Name</h6>
                <h6 class=" text-uppercase ProductPriceLH">RS. 5000</h6>
                  <small class="text-center"><del class="text-danger">4000</del> (10%)</small>
                  <br>
                  <a href="#" class="btn btn-warning  hiddenCartBtn mt-4" id="ATC">Add to Cart</a>
                </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-xs-12" >
                <div class="card card-lift--hover cardlifthover shadow border-0" id="mm" style="">

                  <small class="review" style=" line-height: 0.6; " >
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                    <span class="digits"> (3)</span>
                  </small>
                  <div class="icon icon-shape icon-shape-warning rounded-circle mb-4 PIC">
                    <i class="fa fa-flag" ></i>
                  </div>
                  <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle mb-4 PIC">
                    <i class="ni ni-favourite-28"></i>
                  </div>


                    <img class="card-img-top" src="assets/img/theme/team-1-800x800.jpg" alt="Card image cap">
                    <div class="card-body text-center">

                      <h6 class=" text-uppercase text-center ProductNameLH" >Product Name</h6>
                    <h6 class=" text-uppercase ProductPriceLH">RS. 5000</h6>
                      <small class="text-center"><del class="text-danger">4000</del> (10%)</small>
                      <br>
                      <a href="#" class="btn btn-warning  hiddenCartBtn mt-4 " id="ATC">Add to Cart</a>
                    </div>
                    </div>
                  </div>
                  <div class="col-lg-4" >
                    <div class="card card-lift--hover cardlifthover shadow border-0" id="mm" style="">

                      <small class="review" style=" line-height: 0.6; " >
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <span class="digits"> (3)</span>
                      </small>
                      <div class="icon icon-shape icon-shape-warning rounded-circle mb-4 PIC">
                        <i class="fa fa-flag" ></i>
                      </div>
                      <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle mb-4 PIC">
                        <i class="ni ni-favourite-28"></i>
                      </div>


                        <img class="card-img-top" src="assets/img/theme/team-1-800x800.jpg" alt="Card image cap">
                        <div class="card-body text-center">

                          <h6 class=" text-uppercase text-center ProductNameLH" >Product Name</h6>
                        <h6 class=" text-uppercase ProductPriceLH">RS. 5000</h6>
                          <small class="text-center"><del class="text-danger">4000</del> (10%)</small>
                        
                          <a href="#" class="btn btn-warning  hiddenCartBtn mt-4 " id="ATC">Add to Cart</a>
                        </div>
                        </div>
                      </div>
                      </div>
            <div class="text-center">
          <button class="btn btn-primary">View More Product</button>
            </div>
            </div>   
        </div>
</div><!--reviewTab-->

    </main>
<?php include_once('includes/footer.php'); ?>
    </body>
      </html>
