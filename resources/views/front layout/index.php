<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Design System for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Gotham</title>
  <!-- Favicon -->
  <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="assets/css/custom.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="assets/css/argon.css?v=1.0.1" rel="stylesheet">
  <!-- Docs CSS -->
  <link href="assets/css/layerslider.css" rel="stylesheet" />
  <link href="assets/css/HardCodeStyle.css" rel="stylesheet" />

</head>

<body>
<?php include_once('includes/topNavBar.php');?>
  <main>

    <div class="off-canvas-wrap" data-offcanvas>
     <div class="inner-wrap">

    <div id="layerslider-container-fw">
    <div id="layerslider" style="width: 100%; height: 250px; margin: 0px auto; ">


    <div class="ls-layer" style="slidedirection: right; transition2d: 110,111,112,113; ">
     <img src="assets/slides/MTCET.JPG" class="ls-bg" alt="Slide background">
    <h4 class="ls-s-1 plus2" style=" top:350px; left: 400px; color:white; padding:0.5em; background:#fb6340; slidedirection : fade; slideoutdirection : fade; durationin : 750; durationout : 750; easingin : easeOutQuint; easingout : easeInOutQuint; delayin : 0; delayout : 0; rotatein : 90; rotateout : -90; scalein : .5; scaleout : .5; showuntil : 4000;"> <a href="" style="color:white;">International conference on “Modern Trends in Chemistry and Energy Technologies” - More Images</a></h4>
    </div>

    <div class="ls-layer" style="slidedirection: right; transition2d: 110,111,112,113; ">
     <img src="assets/slides/cj-pm.jpg" class="ls-bg" alt="Slide background">
    <h3 class="ls-s-1 plus2" style=" top:350px; left: 400px; color:#fff; padding:0.5em; background:#fb6340; slidedirection : fade; slideoutdirection : fade; durationin : 750; durationout : 750; easingin : easeOutQuint; easingout : easeInOutQuint; delayin : 0; delayout : 0; rotatein : 90; rotateout : -90; scalein : .5; scaleout : .5; showuntil : 4000;" > <a href=""  style="color:white;" target="_blank">See More</a></h3>
    </div>

    <div class="ls-layer" style="slidedirection: right; transition2d: 110,111,112,113; ">
     <img src="assets/slides/ISSB-2018.jpg" class="ls-bg" alt="Slide background">
    </div>

    <div class="ls-layer" style="slidedirection: right; transition2d: 110,111,112,113; ">
     <img src="assets/slides/BORRN.jpg" class="ls-bg" alt="Slide background">
    </div>

    <div class="ls-layer" style="slidedirection: right; transition2d: 110,111,112,113; ">
     <img src="assets/slides/ISSB2018.jpg" class="ls-bg" alt="Slide background">
    </div>

    <div class="ls-layer" style="slidedirection: right; transition2d: 110,111,112,113; ">
     <img src="assets/slides/ISAEM2018.png" class="ls-bg" alt="Slide background">
     <h3 class="ls-s-1 plus2" style=" top:40px; left: 0px; color:#fff; padding:0.5em; background:#fb6340; slidedirection : fade; slideoutdirection : fade; durationin : 750; durationout : 750; easingin : easeOutQuint; easingout : easeInOutQuint; delayin : 0; delayout : 0; rotatein : 90; rotateout : -90; scalein : .5; scaleout : .5; showuntil : 4000;">International Symposium on Advanced Energy Materials: Production to Storage (ISAEM 2018)    </h3>
     <p class="ls-s-1 text2" style=" top:100px; left: 0px; color:#fff; padding:0.5em; background:#fb6340; slidedirection : fade; slideoutdirection : fade; durationout : 750; easingin : easeOutQuint; delayin : 300; scalein : .8; scaleout : .8; showuntil : 4000;">Interdisciplinary Research Centre in Biomedical Materials (IRCBM)  </p>
    </div>

    <div class="ls-layer" style="slidedirection: right; transition2d: 110,111,112,113; ">
     <img src="assets/slides/EMS.jpg" class="ls-bg" alt="Slide background">
      <h3 class="ls-s-1 plus2" style=" top:40px; left: 0px; color:#fff; padding:0.5em; background:#fb6340; slidedirection : fade; slideoutdirection : fade; durationin : 750; durationout : 750; easingin : easeOutQuint; easingout : easeInOutQuint; delayin : 0; delayout : 0; rotatein : 90; rotateout : -90; scalein : .5; scaleout : .5; showuntil : 4000;">Engineers Meet Scientists day - II   </h3>
     <p class="ls-s-1 text2" style=" top:100px; left: 0px; color:#fff; padding:0.5em; background:#fb6340; slidedirection : fade; slideoutdirection : fade; durationout : 750; easingin : easeOutQuint; delayin : 300; scalein : .8; scaleout : .8; showuntil : 4000;">Department of Physics </p>


    </div>

    <div class="ls-layer" style="slidedirection: right; transition2d: 110,111,112,113; ">
     <img src="assets/slides/2.jpg" class="ls-bg" alt="Slide background">
     <h3 class="ls-s-1 plus2" style=" top:40px; left: 0px; color:#fff; padding:0.5em; background:#fb6340; slidedirection : fade; slideoutdirection : fade; durationin : 750; durationout : 750; easingin : easeOutQuint; easingout : easeInOutQuint; delayin : 0; delayout : 0; rotatein : 90; rotateout : -90; scalein : .5; scaleout : .5; showuntil : 4000;">International Symposium on Modern Trends in Nanotechnology    </h3>
     <p class="ls-s-1 text2" style=" top:100px; left: 0px; color:#fff; padding:0.5em; background:#fb6340; slidedirection : fade; slideoutdirection : fade; durationout : 750; easingin : easeOutQuint; delayin : 300; scalein : .8; scaleout : .8; showuntil : 4000;">Department of Physics </p>
    </div>
    <div class="ls-layer" style="slidedirection: right; transition2d: 110,111,112,113; ">
     <img src="assets/slides/CIMPA2018.jpg" class="ls-bg" alt="Slide background">
    <h3 class="ls-s-1 plus2" style=" top:300px; left: 0px; color:#fff; padding:0.5em; background:#fb6340; slidedirection : fade; slideoutdirection : fade; durationin : 750; durationout : 750; easingin : easeOutQuint; easingout : easeInOutQuint; delayin : 0; delayout : 0; rotatein : 90; rotateout : -90; scalein : .5; scaleout : .5; showuntil : 4000;">CIMPA 2018, School on Combinatorial Commutative Algebra</h3>
    </div>
    <div class="ls-layer" style="slidedirection: right; transition2d: 110,111,112,113; ">
     <img src="assets/slides/2000.jpg" class="ls-bg" alt="Slide background">

    </div>
    <div class="ls-layer" style="slidedirection: right; transition2d: 110,111,112,113; ">
     <img src="assets/slides/ESSD2018.jpg" class="ls-bg" alt="Slide background">

    </div>
    <div class="ls-layer" style="slidedirection: right; transition2d: 110,111,112,113; ">
     <img src="assets/slides/GFIF-2018-2.jpg" class="ls-bg" alt="Slide background">

    </div>
    <div class="ls-layer" style="slidedirection: right; transition2d: 110,111,112,113; ">
     <img src="assets/slides/GFIF-2018-1.jpg" class="ls-bg" alt="Slide background">

    </div>
    <div class="ls-layer" style="slidedirection: right; transition2d: 110,111,112,113; ">
     <img src="assets/slides/GFIF-2018-3.jpg" class="ls-bg" alt="Slide background">

    </div>


    <div class="ls-layer" style="slidedirection: right; transition2d: 110,111,112,113; ">
     <img src="assets/slides/imo2017.jpg" class="ls-bg" alt="Slide background">
     <h3 class="ls-s-1 plus2" style=" top:300px; left: 0px; color:#fff; padding:0.5em; background:#fb6340; slidedirection : fade; slideoutdirection : fade; durationin : 750; durationout : 750; easingin : easeOutQuint; easingout : easeInOutQuint; delayin : 0; delayout : 0; rotatein : 90; rotateout : -90; scalein : .5; scaleout : .5; showuntil : 4000;">1 <sup>st</sup> Training Camp of International Mathematical Olympiad (IMO) 2018</h3>
     <p class="ls-s-1 text2" style=" top:350px; left: 0px; color:#fff; padding:0.5em; background:#fb6340; slidedirection : fade; slideoutdirection : fade; durationout : 750; easingin : easeOutQuint; delayin : 300; scalein : .8; scaleout : .8; showuntil : 4000;">"Organized by Department of Mathematics”</p>

    </div>
    <div class="ls-layer" style="slidedirection: right; transition2d: 110,111,112,113; ">
     <img src="assets/slides/lhr2.jpg" class="ls-bg" alt="Slide background">
     <h3 class="ls-s-1 plus2" style=" top:40px; left: 0px; color:#fff; padding:0.5em; background:#fb6340; slidedirection : fade; slideoutdirection : fade; durationin : 750; durationout : 750; easingin : easeOutQuint; easingout : easeInOutQuint; delayin : 0; delayout : 0; rotatein : 90; rotateout : -90; scalein : .5; scaleout : .5; showuntil : 4000;">Welcome to COMSATS University Islamabad, Lahore Campus </h3>
     <p class="ls-s-1 text2" style=" top:100px; left: 0px; color:#fff; padding:0.5em; background:#fb6340; slidedirection : fade; slideoutdirection : fade; durationout : 750; easingin : easeOutQuint; delayin : 300; scalein : .8; scaleout : .8; showuntil : 4000;">1.5 KM Defence Road, Off Raiwind, Lahore, Pakistan</p>

    </div>



    </div>
    </div>
         </div></div>
         <br><br>



    <section class="section section-lg pt-lg-0">
      <div class="container">
        <div class="row justify-content-center">
          <h1 class="display-2 display2 text-center">Latest Product</h1>
          <div class="col-lg-12">
            <div class="row row-grid">


              <div class="col-lg-3 col-md-4 col-xs-12" >
                <div class="card card-lift--hover cardlifthover shadow border-0" id="mm" style="">

                  <small class="review" style=" line-height: 0.6; " >
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                    <span class="digits"> (3)</span>
                  </small>
                  <div class="icon icon-shape icon-shape-warning rounded-circle mb-4 PIC">
                    <i class="fa fa-flag" ></i>
                  </div>
                  <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle mb-4 PIC">
                    <i class="ni ni-favourite-28"></i>
                  </div>


                    <img class="card-img-top" src="assets/img/theme/team-1-800x800.jpg" alt="Card image cap">
                    <div class="card-body text-center">

                      <h6 class=" text-uppercase text-center ProductNameLH" >Product Name</h6>
                    <h6 class=" text-uppercase ProductPriceLH">RS. 5000</h6>
                      <small class="text-center"><del class="text-danger">4000</del> (10%)</small>
                      <br>
                      <a href="#" class="btn btn-warning  hiddenCartBtn mt-4" id="ATC">Add to Cart</a>
                    </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-4 col-xs-12" >
                    <div class="card card-lift--hover cardlifthover shadow border-0" id="mm" style="">

                      <small class="review" style=" line-height: 0.6; " >
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <span class="digits"> (3)</span>
                      </small>
                      <div class="icon icon-shape icon-shape-warning rounded-circle mb-4 PIC">
                        <i class="fa fa-flag" ></i>
                      </div>
                      <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle mb-4 PIC">
                        <i class="ni ni-favourite-28"></i>
                      </div>


                        <img class="card-img-top" src="assets/img/theme/team-1-800x800.jpg" alt="Card image cap">
                        <div class="card-body text-center">

                          <h6 class=" text-uppercase text-center ProductNameLH" >Product Name</h6>
                        <h6 class=" text-uppercase ProductPriceLH">RS. 5000</h6>
                          <small class="text-center"><del class="text-danger">4000</del> (10%)</small>
                          <br>
                          <a href="#" class="btn btn-warning  hiddenCartBtn mt-4 " id="ATC">Add to Cart</a>
                        </div>
                        </div>
                      </div>
                      <div class="col-lg-3" >
                        <div class="card card-lift--hover cardlifthover shadow border-0" id="mm" style="">

                          <small class="review" style=" line-height: 0.6; " >
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="digits"> (3)</span>
                          </small>
                          <div class="icon icon-shape icon-shape-warning rounded-circle mb-4 PIC">
                            <i class="fa fa-flag" ></i>
                          </div>
                          <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle mb-4 PIC">
                            <i class="ni ni-favourite-28"></i>
                          </div>


                            <img class="card-img-top" src="assets/img/theme/team-1-800x800.jpg" alt="Card image cap">
                            <div class="card-body text-center">

                              <h6 class=" text-uppercase text-center ProductNameLH" >Product Name</h6>
                            <h6 class=" text-uppercase ProductPriceLH">RS. 5000</h6>
                              <small class="text-center"><del class="text-danger">4000</del> (10%)</small>
                              <br>
                              <a href="#" class="btn btn-warning  hiddenCartBtn mt-4 " id="ATC">Add to Cart</a>
                            </div>
                            </div>
                          </div>
                          <div class="col-lg-3 col-md-4 col-xs-12" >
                            <div class="card card-lift--hover cardlifthover shadow border-0" id="mm" style="">

                              <small class="review" style=" line-height: 0.6; " >
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <span class="digits"> (3)</span>
                              </small>
                              <div class="icon icon-shape icon-shape-warning rounded-circle mb-4 PIC">
                                <i class="fa fa-flag" ></i>
                              </div>
                              <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle mb-4 PIC">
                                <i class="ni ni-favourite-28"></i>
                              </div>


                                <img class="card-img-top" src="assets/img/theme/team-1-800x800.jpg" alt="Card image cap">
                                <div class="card-body text-center">

                                  <h6 class=" text-uppercase text-center ProductNameLH" >Product Name</h6>
                                <h6 class=" text-uppercase ProductPriceLH">RS. 5000</h6>
                                  <small class="text-center"><del class="text-danger">4000</del> (10%)</small>
                                  <br>
                                  <a href="#" class="btn btn-warning  hiddenCartBtn mt-4 " id="ATC">Add to Cart</a>
                                </div>
                                </div>
                              </div>



            </div>
            <div class="text-center" style="padding-top:5%;">
              <button class="btn btn-primary">View More Product</button>
            </div>

          </div>
        </div>

      </div>
    </section>


    <section class="section section-lg pt-lg-0">
      <div class="container">
        <div class="row justify-content-center">
          <h1 class="display-2 display2 text-center">Our Services</h1>
          <div class="col-lg-12">
            <div class="row row-grid">


              <div class="col-lg-3 text-center ">
                <div class="card card-lift--hover cardlifthover shadow border-0 "  >
                    <i class="ni ni-delivery-fast delivery-fast"></i>
                    <small class=" text-center"> Fast Delivery</small>
                    <br>

                    </div>
                  </div>

                  <div class="col-lg-3 text-center ">
                    <div class="card card-lift--hover cardlifthover shadow border-0 "  >
                        <i class="fa fa-phone phone" >  </i>
                        <small  class="text-center">24/7 Customer Support</small>
                        <br>
                        </div>
                      </div>
                      <div class="col-lg-3 text-center ">
                        <div class="card card-lift--hover cardlifthover shadow border-0 ">
                            <i class="fa fa-refresh back"> </i>
                            <small  class=" text-center"> Money Back</small>
                            <br>

                            </div>
                          </div>
                          <div class="col-lg-3 text-center ">
                            <div class="card card-lift--hover cardlifthover shadow border-0 ">
                                <i class="fa fa-credit-card payment"> </i>
                                <small  class="text-center"> Online Secure Payment</small>
                                  <br>
                                </div>
                              </div>


            </div>
          </div>
        </div>
      </div>
    </section>

    <h1 class="display-2 display2 text-center">Top Product</h1>
    <div id="exampleSlider">
          <div class="MS-content" >


                          <div class="col-lg-3 col-md-4 col-xs-12 item" >
                            <div class="card card-lift--hover cardlifthover shadow border-0" id="mm" >
                              <div class="row" >


                                <div class="PIC text-center mutliSliderI" >
                              <div class="icon icon-shape icon-shape-warning rounded-circle mb-4" >
                                <i class="fa fa-flag" ></i>
                              </div>
                              <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle mb-4 ">
                                <i class="ni ni-favourite-28"></i>
                              </div>
                              </div>
                            </div>

                                <img class="card-img-top" src="assets/img/theme/team-1-800x800.jpg" alt="Card image cap">
                                <div class="card-body text-center">


                                    <h6 class=" text-uppercase text-center ProductNameLH" >Product Name</h6>



                                    <small style="  line-height: 0.2;">
                                      <span class="fa fa-star checked"></span>
                                      <span class="fa fa-star checked"></span>
                                      <span class="fa fa-star checked"></span>
                                      <span class="fa fa-star"></span>
                                      <span class="fa fa-star"></span>
                                      <span class="digits"> (3)</span>
                                    </small>

                                    <h6 class=" text-uppercase ProductPriceLH">RS. 5000</h6>
                                  <small class="text-center"><del class="text-danger">4000</del> (10%)</small>
                                  <br>
                                  <a href="#" class="btn btn-warning  hiddenCartBtn mt-4" id="ATC">Add to Cart</a>
                                </div>
                                </div>
                              </div>
                              <div class="col-lg-3 col-md-4 col-xs-12 item" >
                                <div class="card card-lift--hover cardlifthover shadow border-0" id="mm" >
                                  <div class="row" >


                                    <div class="PIC text-center mutliSliderI" >
                                  <div class="icon icon-shape icon-shape-warning rounded-circle mb-4" >
                                    <i class="fa fa-flag" ></i>
                                  </div>
                                  <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle mb-4 ">
                                    <i class="ni ni-favourite-28"></i>
                                  </div>
                                  </div>
                                </div>

                                    <img class="card-img-top" src="assets/img/theme/team-1-800x800.jpg" alt="Card image cap">
                                    <div class="card-body text-center">

                                      <h6 class=" text-uppercase text-center ProductNameLH" >Product Name</h6>



                                      <small style="  line-height: 0.2;">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="digits"> (3)</span>
                                      </small>

                                      <h6 class=" text-uppercase ProductPriceLH">RS. 5000</h6>
                                      <small class="text-center"><del class="text-danger">4000</del> (10%)</small>
                                      <br>
                                      <a href="#" class="btn btn-warning  hiddenCartBtn mt-4" id="ATC">Add to Cart</a>
                                    </div>
                                    </div>
                                  </div>
                                  <div class="col-lg-3 col-md-4 col-xs-12 item" >
                                    <div class="card card-lift--hover cardlifthover shadow border-0" id="mm" >
                                      <div class="row" >
                                        <div class="PIC text-center mutliSliderI" >
                                      <div class="icon icon-shape icon-shape-warning rounded-circle mb-4" >
                                        <i class="fa fa-flag" ></i>
                                      </div>
                                      <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle mb-4 ">
                                        <i class="ni ni-favourite-28"></i>
                                      </div>
                                      </div>
                                    </div>

                                        <img class="card-img-top" src="assets/img/theme/team-1-800x800.jpg" alt="Card image cap">
                                        <div class="card-body text-center">

                                          <h6 class=" text-uppercase text-center ProductNameLH" >Product Name</h6>



                                          <small style="  line-height: 0.2;">
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="digits"> (3)</span>
                                          </small>

                                          <h6 class=" text-uppercase ProductPriceLH">RS. 5000</h6>
                                          <small class="text-center"><del class="text-danger">4000</del> (10%)</small>
                                          <br>
                                          <a href="#" class="btn btn-warning  hiddenCartBtn mt-4" id="ATC">Add to Cart</a>
                                        </div>
                                        </div>
                                      </div>
                                      <div class="col-lg-3 col-md-4 col-xs-12 item" >
                                        <div class="card card-lift--hover cardlifthover shadow border-0" id="mm" >
                                          <div class="row" >

                                        <div class="PIC text-center" style=" padding-left:30%; padding-top:10%">
                                          <div class="icon icon-shape icon-shape-warning rounded-circle mb-4" >
                                            <i class="fa fa-flag" ></i>
                                          </div>
                                          <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle mb-4 ">
                                            <i class="ni ni-favourite-28"></i>
                                          </div>
                                          </div>
                                        </div>

                                            <img class="card-img-top" src="assets/img/theme/team-1-800x800.jpg" alt="Card image cap">
                                            <div class="card-body text-center">

                                              <h6 class=" text-uppercase text-center ProductNameLH" >Product Name</h6>



                                              <small style="  line-height: 0.2;">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="digits"> (3)</span>
                                              </small>

                                              <h6 class=" text-uppercase ProductPriceLH">RS. 5000</h6>
                                              <small class="text-center"><del class="text-danger">4000</del> (10%)</small>
                                              <br>
                                              <a href="#" class="btn btn-warning  hiddenCartBtn mt-4" id="ATC">Add to Cart</a>
                                            </div>
                                            </div>
                                          </div>
                                          <div class="col-lg-3 col-md-4 col-xs-12 item" >
                                            <div class="card card-lift--hover cardlifthover shadow border-0" id="mm">
                                              <div class="row" >

                                            <div class="PIC text-center mutliSliderI" >
                                              <div class="icon icon-shape icon-shape-warning rounded-circle mb-4" >
                                                <i class="fa fa-flag" ></i>
                                              </div>
                                              <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle mb-4 ">
                                                <i class="ni ni-favourite-28"></i>
                                              </div>
                                              </div>
                                            </div>

                                                <img class="card-img-top" src="assets/img/theme/team-1-800x800.jpg" alt="Card image cap">
                                                <div class="card-body text-center">

                                                  <h6 class=" text-uppercase text-center ProductNameLH" >Product Name</h6>



                                                  <small style="  line-height: 0.2;">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="digits"> (3)</span>
                                                  </small>

                                                  <h6 class=" text-uppercase ProductPriceLH">RS. 5000</h6>
                                                  <small class="text-center"><del class="text-danger">4000</del> (10%)</small>
                                                  <br>
                                                  <a href="#" class="btn btn-warning  hiddenCartBtn mt-4" id="ATC">Add to Cart</a>
                                                </div>
                                                </div>
                                              </div>
                                              <div class="col-lg-3 col-md-4 col-xs-12 item" >
                                                <div class="card card-lift--hover cardlifthover shadow border-0" id="mm" >
                                                  <div class="row" >

                                                  <div class="PIC text-center mutliSliderI" >
                                                  <div class="icon icon-shape icon-shape-warning rounded-circle mb-4" >
                                                    <i class="fa fa-flag" ></i>
                                                  </div>
                                                  <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle mb-4 ">
                                                    <i class="ni ni-favourite-28"></i>
                                                  </div>
                                                  </div>
                                                </div>

                                                    <img class="card-img-top" src="assets/img/theme/team-1-800x800.jpg" alt="Card image cap">
                                                    <div class="card-body text-center">

                                                      <h6 class=" text-uppercase text-center ProductNameLH" >Product Name</h6>



                                                      <small style="  line-height: 0.2;">
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="digits"> (3)</span>
                                                      </small>

                                                      <h6 class=" text-uppercase ProductPriceLH">RS. 5000</h6>
                                                      <small class="text-center"><del class="text-danger">4000</del> (10%)</small>
                                                      <br>
                                                      <a href="#" class="btn btn-warning  hiddenCartBtn mt-4" id="ATC">Add to Cart</a>
                                                    </div>
                                                    </div>
                                                  </div>
                                                  <div class="col-lg-3 col-md-4 col-xs-12 item" >
                                                    <div class="card card-lift--hover cardlifthover shadow border-0" id="mm" >
                                                      <div class="row" >

                                                    <div class="PIC text-center mutliSliderI" >
                                                      <div class="icon icon-shape icon-shape-warning rounded-circle mb-4" >
                                                        <i class="fa fa-flag" ></i>
                                                      </div>
                                                      <div class="icon icon-shape icon-shape-primary iconPrimary rounded-circle mb-4 ">
                                                        <i class="ni ni-favourite-28"></i>
                                                      </div>
                                                      </div>
                                                    </div>

                                                        <img class="card-img-top" src="assets/img/theme/team-1-800x800.jpg" alt="Card image cap">
                                                        <div class="card-body text-center">

                                                          <h6 class=" text-uppercase text-center ProductNameLH" >Product Name</h6>



                                                          <small style="  line-height: 0.2;">
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star checked"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="digits"> (3)</span>
                                                          </small>

                                                          <h6 class=" text-uppercase ProductPriceLH">RS. 5000</h6>
                                                          <small class="text-center"><del class="text-danger">4000</del> (10%)</small>
                                                          <br>
                                                          <a href="#" class="btn btn-warning  hiddenCartBtn mt-4" id="ATC">Add to Cart</a>
                                                        </div>
                                                        </div>
                                                      </div>


          </div>
          <div class="MS-controls">
              <button class="MS-left"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
              <button class="MS-right"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
          </div>
      </div>



  </main>
<?php include_once('includes/footer.php'); ?>

  <body>
    </html>
